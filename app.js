const loanAmount = document.getElementById("loanValue");
const salaryAmount = document.getElementById("salaryAmount");
const computersList = document.getElementById("computers");
const computersFeatures = document.getElementById("laptopFeatures");
const computersImage = document.getElementById("laptopImage");
const computersName = document.getElementById("laptopName");
const computersDescription = document.getElementById("laptopDescription");
const computersPrice = document.getElementById("laptopsPrice");
const loanButtonContainer = document.getElementById("loanButtonContainer");

let loan = 0.0;
let salary = 0.0;
let savings = 0.0;
let hasDebt = false;

let computers = [];

const getLoan = () => {
  const loanAmount = document.getElementById("loanValue");
  const savingsAmount = document.getElementById("savingsValue");
  let currentLoan = 0.0;
  currentLoan = parseInt(prompt("How much do you want to loan?"));
  if (loan > 0) {
    alert("Need to downpay current loan before you can get a new one!");
  } else if (currentLoan > savings * 2) {
    alert("You can only get a loan 2X your current savings");
  } else {
    loan += currentLoan;
    hasDebt = true;
    loanAmount.textContent = loan;
    savings += currentLoan;
    savingsAmount.innerHTML = savings;

    //Generate downpayment button
    const downPayButton = document.createElement("button");
    downPayButton.setAttribute("class", "loanButtons");
    downPayButton.setAttribute("id", "downpayButton");
    downPayButton.setAttribute("onclick", "downpayment()");
    downPayButton.appendChild(document.createTextNode("Full pay downpayment"));
    loanButtonContainer.appendChild(downPayButton);
  }
};

const work = () => {
  const salaryAmount = document.getElementById("salaryValue");
  salary += 100;
  salaryAmount.innerHTML = salary;
};

let rest = 0;
const save = () => {
  const savingsAmount = document.getElementById("savingsValue");
  const salaryAmount = document.getElementById("salaryValue");
  const loanAmount = document.getElementById("loanValue");
  if (loan == 0) {
    //Update savings
    savings += salary;
    savingsAmount.innerHTML = savings;
  } else {
    if (loan - salary * 0.1 >= 0) {
      //Subtract 10% of debt, unless 10% exceeds debt
      loan -= salary * 0.1;
      //Add rest to savings
      savings += salary * 0.9;
    } else {
      savings += salary * 0.1 - loan;
      savings += salary * 0.9;
      loan = 0;
    }

    loanAmount.innerHTML = loan;
    savingsAmount.innerHTML = savings;
  }
  //Reset salary html and variable
  salary = 0;
  salaryAmount.innerHTML = salary;
};

const downpayment = () => {
  const savingsAmount = document.getElementById("savingsValue");
  const salaryAmount = document.getElementById("salaryValue");
  const loanAmount = document.getElementById("loanValue");
  const downpayButton = document.getElementById("downpayButton");
  //If salary exceeds loan value, add rest to savings
  if (loan - salary < 0) {
    let tmp = salary - loan;
    savings += tmp;
    loan = 0;
    salary = 0;
    downpayButton.remove();
  } else {
    //Subtract salary from loan
    loan -= salary;
    salary = 0;
  }
  savingsAmount.innerHTML = savings;
  loanAmount.innerHTML = loan;
  salaryAmount.innerHTML = salary;
};

const buyLaptop = () => {
  const savingsAmount = document.getElementById("savingsValue");
  if (savings - computersPrice.innerHTML > 0) {
    savings -= computersPrice.innerHTML;
    savingsAmount.innerHTML = savings;
    alert("You have purchased this laptop!");
  } else {
    alert("Cannot buy this laptop not enough funds");
  }
};

let URL = "https://noroff-komputer-store-api.herokuapp.com/computers";

fetch(URL)
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

const addComputersToMenu = (computers) => {
  computers.forEach((computer) => addComputerToMenu(computer));
};

const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersList.appendChild(computerElement);
};

const addComputerFeaturesToList = (featuresArray) => {
  featuresArray.forEach((feature) => addComputerFeatureToList(feature));
};

const addComputerFeatureToList = (feature) => {
  const computerFeature = document.createElement("li");
  computerFeature.appendChild(document.createTextNode(feature));
  computersFeatures.appendChild(computerFeature);
};

const handleComputerFeaturesChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  computersFeatures.innerHTML = "";
  addComputerFeaturesToList(selectedComputer.specs);
};

const handleComputerNameChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  computersName.innerText = selectedComputer.title;
};

const handleComputerDescriptionChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  computersDescription.innerHTML = "";
  computersDescription.innerText = selectedComputer.description;
};

let imageURLBase = "https://noroff-komputer-store-api.herokuapp.com/";
let imageURL = "";
const handleComputerImageChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  imageURL = imageURLBase + selectedComputer.image;
  computersImage.src = imageURL;
};

const handleComputerPriceChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  computersPrice.innerText = selectedComputer.price;
};

computersList.addEventListener("change", handleComputerFeaturesChange);
computersList.addEventListener("change", handleComputerNameChange);
computersList.addEventListener("change", handleComputerDescriptionChange);
computersList.addEventListener("change", handleComputerImageChange);
computersList.addEventListener("change", handleComputerPriceChange);
