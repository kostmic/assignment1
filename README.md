# Assignment1 - Computer store 💻

## Description
This is a website which gets JSON data from https://noroff-komputer-store-api.herokuapp.com/computers  and displays it. Aswell as display information about each computer it also has simple logic which lets you "work", take a loan and eventually buy any of the displayed computers. 

## Hosting
The website is hosted at: https://kostmic.gitlab.io/assignment1/

